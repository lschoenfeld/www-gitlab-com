---
layout: handbook-page-toc
title: "Marketing Programs Management - MPM"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Marketing Program Managers

Marketing Program Managers focus on executing, measuring and scaling GitLab's marketing campaigns, landing a message strategically focusing on a target audience using channels such as email nurture, digital ads, paid and organic social, events, and more. Marketing programs works with Content Marketing and Product Marketing to activate content in the most effective manner to drive leads for SDRs and Sales. Webcasts, gated content, and Pathfactory strategy/best practices are owned by the Digital Marketing Programs team.

### Key Responsibilities & Quick Links
* [Integrated Campaigns](/handbook/marketing/campaigns/)
    * [Active Integrated Campaigns](/handbook/marketing/campaigns/#active-integrated-campaigns)
    * [Upcoming and Future Integrated Campaigns](https://gitlab.com/groups/gitlab-com/marketing/-/epics/749)
    * [Past Integrated Campaigns](/handbook/marketing/campaigns/#past-integrated-campaigns)
    * [Campaign Planning](/handbook/marketing/campaigns/#campaign-planning)
* [Emails & Nurture Programs](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/emails-nurture)
    * [Nurture Programs](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/emails-nurture/#email-nurture-programs)
    * [Newsletter](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/emails-nurture/#newsletter)
    * [Ad Hoc Emails](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/emails-nurture/#ad-hoc-one-time-emails---requesting-an-email)
* [Virtual Events](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/)
    * [Webcasts](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/webcast/)
    * [Virtual Sponsorships](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/#sponsored-virtual-events)
    * [Self-Service Virtual Event](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/meetings-with-without-breakout/)
* [Gated Content](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/gated-content)
    * [Resource Center (External)](https://about.gitlab.com/resources/)
    * [Internal Library of Links](https://docs.google.com/spreadsheets/d/1NK_0Lr0gA0kstkzHwtWx8m4n-UwOWWpK3Dbn4SjLu8I/edit#gid=0)
* [Events (MPM Processes)](/handbook/marketing/events/#mpm-steps-to-set-up-event-epic)

## The Marketing Programs Team

**Jackie Gragnola** *Manager, Marketing Programs*
* **Team Prioritization**: plan prioritization of campaigns, related content and webcasts, event support, and projects for the team
* **Hiring**: organize rolling hiring plan to scale with organization growth
* **Onboarding**: create smooth and effective onboarding experience for new team members to ramp quickly and take on responsibilities on the team
* **Transition of Responsibilities**: plan for and organize efficient handoff to new team members and between team members when prioritization changes occur

**Agnes Oetama** *Sr. Marketing Program Manager*
* **Integrated Campaigns**: organize execution schedule, timeline, and DRIs for integrated campaigns.
* **Virtual Events**: project management, set up, promotion, and follow up of all virtual events (webcasts, demos, virtual sponsorship)
* **Bi-weekly Newsletter**: coordinate with cross-functional teams on topics and set up newsletter in marketo
* **APAC Field Event Support**: campaign tracking, landing pages, invitations, follow up, and reporting of all events (working closely with Field Marketing team)
* **APAC Corporate Event Support**: campaign tracking, landing pages, invitations, follow up, and reporting of all events (working closely with Corporate Events and Alliances teams)

**Jenny Tiemann** *Sr. Marketing Program Manager*
* **Integrated Campaigns**: organize execution schedule, timeline, and DRIs for integrated campaigns.
* **NORAM - Central & PubSec Field Event Support**: campaign tracking, landing pages, invitations, follow up, and reporting of all events (working closely with Field Marketing team)
* **Acceleration and ABM Campaigns**: organize execution, timeline, and campaign tracking.
* **Nurture Campaigns**: strategize and set up campaigns (email nurturing)

**Zac Badgley** *Sr. Marketing Program Manager*
* **Integrated Campaigns**: organize execution schedule, timeline, and DRIs for integrated campaigns.
* **NORAM - East Field Event Support**: campaign tracking, landing pages, invitations, follow up, and reporting of all events (working closely with Field Marketing team)
* **Analyst Content:** collaborate with Analyst Relations on assets to be gated, sunsetted, and organize opportunities for future content

**Nout Boctor-Smith** *Sr. Marketing Program Manager*
* **Integrated Campaigns**: organize execution schedule, timeline, and DRIs for integrated campaigns.
* **Corporate Worldwide Event Support**: campaign tracking, landing pages, invitations, follow up, and reporting of all events (working closely with Corporate Events and Alliances teams)
* **Ad-Hoc Emails**: coordination of copy, review, and set up of one-time emails (i.e. security alert emails, package/pricing changes)

**Megan Mitchell** *Sr. Marketing Program Manager*
* **Integrated Campaigns**: organize execution schedule, timeline, and DRIs for integrated campaigns.
* **NORAM - West Field Event Support**: campaign tracking, landing pages, invitations, follow up, and reporting of all events (working closely with Field Marketing team)

**Eirini Panagiotopoulou** *Sr. Marketing Program Manager*
* **Integrated Campaigns**: organize execution schedule, timeline, and DRIs for integrated campaigns.
* **Corporate EMEA Event Support**: campaign tracking, landing pages, invitations, follow up, and reporting of all events (working closely with Corporate Events and Alliances teams
* **EMEA - Southern Europe & MEA Field Event Support**: campaign tracking, landing pages, invitations, follow up, and reporting of all events (working closely with Field Marketing team)

**Indre Kryzeviciene** *Marketing Program Manager*
* **Integrated Campaigns**: organize execution schedule, timeline, and DRIs for integrated campaigns.
* **EMEA - UK/I, Northern Europe & Russia, Central Europe & CEE Field Event Support**: campaign tracking, landing pages, invitations, follow up, and reporting of all events (working closely with Field Marketing team

*Each team member contributes to making day-to-day processes more efficient and effective, and will work with marketing operations as well as other relevant teams (including field marketing, content marketing, and product marketing) prior to modification of processes.*

### Holiday coverage for S1 security vulnerabilities email communication

In the event of an S1 (critical) security vulnerability email communication is needed during the holidays, please create an issue using *[Email-Request-mpm template](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/blob/master/.gitlab/issue_templates/Email-Request-mpm.md)* and ping in [#marketing_programs](https://gitlab.slack.com/archives/CCWUCP4MS) tagging @marketing-programs

## Email marketing calendar

The calendar below documents the emails to be sent via Marketo and Mailchimp for:
1. event support (invitations, reminders, and follow ups)
1. ad hoc emails (security, etc.)
1. webcast emails (invitations, reminders, and follow ups)
1. milestones for nurture campaigns (i.e. when started, changed, etc. linking to more details)

*Note: emails in the future may be pushed out if timelines are not met for email testing, receiving lists from event organizers late, etc. The calendar will be updated if the email is pushed out. Please reference the MPM issue boards (described below on this page) to see progress of specific events/webcasts/etc.*

<figure>
  <iframe src="https://calendar.google.com/calendar/b/1/embed?showPrint=0&amp;height=600&amp;wkst=1&amp;bgcolor=%23FFFFFF&amp;src=gitlab.com_bpjvmm7ertrrhmms3r7ojjrku0%40group.calendar.google.com&amp;color=%23B1365F&amp;ctz=America%2FLos_Angeles" style="border-width:0" width="800" height="600" frameborder="0" scrolling="no"></iframe>
</figure>

## Marketo vs. PathFactory

`Note: This section will be phased out on 2020-04-07 and proper links will be added to the top of this handbook page so that the Marketing Programs page focuses on who the team is and reporting, with relevant tactics (integrated campaigns, email and nurture, virtual events, gated content, events, etc. are linked.`

This section has been updated and moved to the [pathfactory handbook page.](/handbook/marketing/marketing-operations/pathfactory/)

# How we work together

## Issue management in GitLab

### How our team stays productive
Below are issue views that the Marketing Program Team monitors for in-progress and upcoming action items:
* [MPM Priority](https://gitlab.com/groups/gitlab-com/marketing/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=MPM%20Priority)
* [Landing Pages](https://gitlab.com/groups/gitlab-com/marketing/-/issues?scope=all&utf8=✓&state=opened&label_name[]=MPM%20-%20Landing%20Page%20%26%20Design)
* [Invitations & Reminder Emails](https://gitlab.com/groups/gitlab-com/marketing/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=MPM%20-%20Invitations%20%26%20Reminder)
* [Follow Up Emails](https://gitlab.com/groups/gitlab-com/marketing/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=MPM%20-%20Follow%20Up%20Emails)
* [Add to Nurture](https://gitlab.com/groups/gitlab-com/marketing/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=MPM%20-%20Add%20to%20Nurture)
* [MPM - Radar](https://gitlab.com/groups/gitlab-com/marketing/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=MPM%20-%20Radar)

### Marketing Programs labels in GitLab

* **Marketing Programs**: General labels to track all issues related to Marketing Programs. This brings the issue into the board for actioning.
* **MPM - Radar**: Holding place for any issues that will need Marketing Program Manager support, including gated content, events, webcasts, etc.
* **MPM - Supporting Epic / Issue Created**: Indicates that the epic and supporting issues were created for the MPM - Radar issue. At the time this label is applied, the "MPM - Radar" label will be removed.
* **MPM - Secure presenters and schedule dry runs**: Used when MPM is securing presenters and Q&A support for an upcoming virtual event.
* **MPM - Landing Page & Design**: Used by Marketing Program Manager to indicate that the initiative is in the stage of landing page creation and requesting design assets from the web/design team.
* **MPM - Marketo Flows**: Used by Marketing Program Manager to indicate that the initiative is in the stage of editing/testing of flows in Marketo.
* **MPM - Create Target List**: Used by Marketing Program Manager requested of Marketing Ops and in collaboration with Field Marketing to receive a list curated for the geo target. Marketo smart lists for larger metro areas around the world are built to expedite list creation. Additional curation done in Salesforce.
* **MPM - Invitations & Reminder**: Used by Marketing Program Manager when the initiative is in the stage of identifying segmentation to target and outreach strategy.
* **MPM - Follow Up Emails**: Used by Marketing Program Manager when initiative is in the stage of writing and reviewing relevant emails (reminders, follow up, etc.).
* **MPM - Add to Nurture**: Used by Marketing Program Manager when initiative is in the stage of being added to nurture.
* **MPM - Project**: For non-campaign based optimizations, ideation, and projects of Marketing Program Managers
* **MPM - Blocked/Waiting**: Designates that the MPM is blocked by another team member from moving forward on the issue.
* **MPM - Switch to On-demand**: Used by Marketing Program Manager when switching webcast landing page and subsequent marketo programs to on-demand post event.
* **MPM Priority**: to be used by MPMs to organize their top priority tasks. These are not to be applied by other team members.

### Tips & Tricks

#### Creating a MacBook shortcut for repetitive statements
**Example:** in an issue to update all of MPM, instead of typing out every name, I have added a shortcut in my computer to populate all of the MPMs GitLab handles when I type `asdf + Enter`.

**How to:**
* On your Mac, choose Apple menu (ever-present top left logo)
* Go to `System Preferences`
* Click the `Keyboard` section
* Click `Text` on the top  nav options
* Cick the `+` at the bottom of the option list
* In `Replace` column, add the shortcut that you would type in to populate the repetitive text
* In `With` column, add the repetitive text that you want to populate when you type in the shortcut

# Marketing Programs support requests

## Requesting to "Gate" a Piece of Content

`Note: This section will be phased out on 2020-04-10 and proper links will be added to the top of this handbook page so that the Marketing Programs page focuses on who the team is and reporting, with relevant tactics (integrated campaigns, email and nurture, virtual events, gated content, events, etc. are linked.`

This section has been updated and moved to the [gated-content page.](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/gated-content)

### A visual of what happens when someone fills out a form on a landing page

![image](/images/handbook/marketing/marketing-programs/landing-pages-flow-model.png)

❌ **A landing page with a form should never be created without the inclusion and testing by Marketing Programs and/or Marketing Ops.**

Please post in the [slack channel #marketing-programs](https://gitlab.slack.com/messages/CCWUCP4MS) if you have any questions.

## Marketing Programs and Virtual (Online) Events support

`Note: This section will be phased out on 2020-04-10 and proper links will be added to the top of this handbook page so that the Marketing Programs page focuses on who the team is and reporting, with relevant tactics (integrated campaigns, email and nurture, virtual events, gated content, events, etc. are linked.`

This section has been updated and moved to the [virtual-events page.](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/)

## Marketing Programs and Event Support

`Note: This section will be phased out on 2020-04-10 and proper links will be added to the top of this handbook page so that the Marketing Programs page focuses on who the team is and reporting, with relevant tactics (integrated campaigns, email and nurture, virtual events, gated content, events, etc. are linked.`

This section has been updated and moved to the [events page.](/handbook/marketing/events/)

Some quick links to specific sections you may be looking for:
* [Campaign tags for events](/handbook/marketing/events/#all-events---setting-up-the-campaign-tag)
* [Timelines and SLAs between Field Marketing and Marketing Programs](/handbook/marketing/events/#timelines-and-slas-between-field-marketing-and-marketing-programs)
* [Timelines and SLAs between Corporate Marketing and Marketing Programs](/handbook/marketing/events/#timelines-and-slas-between-corporate-marketing-and-marketing-programs)
* [MPM process for event epics and related issues](/handbook/marketing/events/#mpm-steps-to-set-up-event-epic)
* [The different event types (conferences, field events, owned events, and speaking sessions)](/handbook/marketing/events/#offline-events-channel-types)
* [Adding an entry to `/events/` page](/handbook/marketing/events/#how-to-add-events-to-aboutgitlabcomevents)
* [Using the Marketo Check-in App](/handbook/marketing/events/#marketo-check-in-app)

## Requesting an Email, Email Nurture, and Best Practices

`Note: This section will be phased out on 2020-04-10 and proper links will be added to the top of this handbook page so that the Marketing Programs page focuses on who the team is and reporting, with relevant tactics (integrated campaigns, email and nurture, virtual events, gated content, events, etc. are linked.`

The following email-related topics and request processes are now included in the [Emails & Nurture Programs handbook page](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/emails-nurture)
* Requesting an email
* Vizualization of current nurture streams
* Top funnel nurture
* Integrated campaign nurture
* SaaS trial nurture
* Self-hosted trial nurture
* Email marketing best practices (including content, design, and A/B testing)
* Requesting to add leads to the top funnel nurture
* Requesting to add a new asset to the top funnel nurture
* top funnel nurture, integrated campaign nurture

# Programs reporting

## Ideal questions to answer
#### Overall (WIP to deliver all)
* What is the pipe-to-spend for our integrated campaigns? How much pipeline are our integrated campaigns generating?
* What is the pipe-to-spend for our tactics (i.e. webcasts, gated content,etc.)? How much pipeline are our different tactics generating?
* Which channels (i.e. paid ads, social, organice) are contributing to the highest quantity AND quality leads?
* Which sources (i.e. webcast, content) are contributing to the highest quantity AND quality leads?
* Which mix of channels and source deliver the optimal pipe-to-spend? Which mix delivers the highest quantity AND quality leads?

#### By Campaign
* What is the pipe-to-spend for X campaign?
* How much pipeline has X campaign generated?
* What is the funnel movement for leads in X campaign? (Raw > Inquiry > MQL > Accepted > Qualifying > Qualified)
* Which mix of channel and source is delivering the highest quantity AND quality leads?
* Which channels are driving the most/least leads in X campaign?
* Which channels are driving the most/least qualified leads in X campaign? (i.e. moving to Accepted vs. Unqualified)
* How many leads from X campaign are being generated for each sales segment?
* How many leads from X campaign are being generated for each sales region?
* What is the breakdown of segment and region for X campaign?
* What are the most common disqualification criteria for leads in X campaign? (analyze Unqualified Reason)

MPM uses a WIP DataStudio dashboard to report on integrated campaign performance. This was created by @aoetama and she is in the process of building into a Sisence dashboard.

These dashboards use Bizible attribution touchpoints, tracking First Touch (FT), Lead Creation (LC), Opportunity Creation, and Opportunity Close.

## Offer-Specific Dashboards

SFDC reports and dashboards to track program performance real-time. Data from the below SFDC reports/dashboards along with anecdotal feedback gathered during program retros will be used as guidelines for developing and growing various marketing programs.

The SFDC report/dashboard is currently grouped by program types so MPMs can easily compare and identify top performing and under performing programs within the areas that they are responsible for.

### Key Metrics tracked in ALL virtual events dashboards

*Note: Virtual Events include Webcast, Live Demos and Virtual Sponsorship*

* **Total Registration :** The number of people that registered for the virtual event regardless whether they attend or not.
* **Total Attendance:** The number of people that attended the LIVE virtual event (exclude people who watched the on-demand version).
* **Attendance Rate:** % of people that attended the LIVE virtual event out of the total registered (i.e: Total Attendance / Total Registration).
* **Net New Names:** The number of net new names added to our marketing database driven by the virtual event. Because a net new person record may be inserted into our CRM (SFDC) as a lead or a contact object therefore, we need to add `Total net new leads` and `Total net new contacts` to get the overall total net new names.
* **Influenced Pipe:** Total New and Add-on business pipeline IACV$ influenced by people who attended the LIVE virtual event. The webcast and live demo dashboards currently use SFDC out of the box `Campaigns with Influenced opportunities` report type because Bizible was implemented in June'18 and therefore the attribution report did not capture data prior to this. We plan to migrate webcast and live demo influenced pipe reports to Bizible attribution report in the next dashboard iteration so they align with overall marketing reporting.

#### Virtual Events Reporting

The [Webcast Dashboard](https://gitlab.my.salesforce.com/01Z6100000079e6) tracks all webcasts hosted on GitLab's internal webcast platform. It is organized into 3 columns. The left and middle columns tracks 2 different webcast series (Release Radar vs. CI/CD webcast series). The right column tracks various one-off webcasts since Jan'18.

The [Live Demo Dashboard](https://gitlab.my.salesforce.com/01Z6100000079f4) is organized into 2 columns. The left column tracks the bi-weekly Enterprise Edition product demos (1 hour duration). The bi-weekly Enterprise Edition product demos ran between Q1'18 - Q2'18.
The right column tracks the weekly high level product demo + Q&A session (30 minutes duration). The weekly high level product demo + Q&A session was launched in Q4'18 and currently running through the end of Feb 2019.

The [Virtual Sponsorship Dashboard](https://gitlab.my.salesforce.com/01Z61000000UD44) focuses on events that are hosted by a 3rd party where GitLab has purchased a virtual booth or sponsorship.

# MPM programs logistical set up (moved to independent pages)

### Webcast

`Note: This section will be phased out on 2020-04-10 and proper links will be added to the top of this handbook page so that the Marketing Programs page focuses on who the team is and reporting, with relevant tactics (integrated campaigns, email and nurture, virtual events, gated content, events, etc. are linked.`

Webcast program set up can be found in the [Business OPS](/handbook/marketing-programs) section of the handbook.

### Newsletter

`Note: This section will be phased out on 2020-04-10 and proper links will be added to the top of this handbook page so that the Marketing Programs page focuses on who the team is and reporting, with relevant tactics (integrated campaigns, email and nurture, virtual events, gated content, events, etc. are linked.`

Bi-weeekly newsletter schedule and process to create and edit can now be found in the [Emails and Nurture handbook page](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/emails-nurture)